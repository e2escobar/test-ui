# test-ui

## TO-DO

* Abstract scss classes.
* Apply DRY over html components.
* Upgrade to Vuex.
* Review and apply media queries to responsive design.


> Test UI Project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```


### 1.	Layout HTML-CSS.
> To make the site architecture I used Vue.js and Bulma css. I create a five-component	 structure based on the main comp.
  Next basic component Diagram I show the structure.
  TuBanner = Test UI Banner = <tu-banner/> Component. 
  
  ![alt text](./com.png "Components")
  
> The css structure was create with sass and bulma.io, a main.scss and variables.scss file and scoped styles on each one component. 
  
 ### 2.	Interaction – JS.
  
>  I just create a main component to loop inside the cards. 
  The logic of the component calculate the percentage of each person and put the clases and colors dynamically.

### 3.	NodeJS and Future work.

>I could create all the backend structure using  sails.js and the api generator, for two models, one of them for authentication and other one for manage the votes information.

>In Front app (Vue), I’ll could manage the application state with veux (Vue redux) and get the data for http requests using services and some api or library like Trae.

![alt text](./arch.png "Architecture")
