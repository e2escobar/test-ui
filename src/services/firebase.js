import Firebase from 'firebase'

const config = {
  apiKey: "AIzaSyD3q1Q0cXiIWr664qGy4Rlh-uRyZeHh1q8",
  authDomain: "test-ui-data.firebaseapp.com",
  databaseURL: "https://test-ui-data.firebaseio.com",
  projectId: "test-ui-data",
  storageBucket: "test-ui-data.appspot.com",
  messagingSenderId: "150506633434"
};

const app = Firebase.initializeApp(config)

export const db = app.database()
