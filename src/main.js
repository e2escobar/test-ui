import '@/services/firebase';

import VueFire from 'vuefire'
import Vue from 'vue'
import App from './App.vue'

Vue.use(VueFire)

new Vue({
  el: '#app',
  render: h => h(App)
})
